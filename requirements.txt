Django==3.0.8
djangorestframework==3.11.0
python-decouple==3.3
dj-database-url==0.5.0
psycopg2-binary==2.8.5
django-extensions==2.2.9
django-localflavor==3.0.1
djangorestframework-camel-case==1.1.2
django-filter==2.3.0
drf-extensions==0.6.0
Pillow==7.2.0
boto3==1.14.32
django-storages==1.9.1
gunicorn
whitenoise
django-heroku
-e git+https://fhsistema:f93tp4512@bitbucket.org/rizzoimobiliaria/sankya-synchronizer.git#egg=sankya-synchronizer
