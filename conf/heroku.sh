#!/bin/bash
python manage.py migrate
python manage.py loaddata /app/project/fixtures/000_site_dev.json
python manage.py loaddata /app/project/fixtures/001_admin.json
python manage.py collectstatic --noinput
python manage.py sync_all
