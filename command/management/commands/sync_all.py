from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import gettext as _


class Command(BaseCommand):
    """ Sincroniza todas as sincronizações do sistema """

    help = _('Sincronizar de todos os dados com a Sankhia')
    requires_migrations_checks = True

    def handle(self, *args, **options):

        try:
            call_command('sync_project')
            call_command('sync_lot')
            call_command('sync_member')
        except Exception as e:
            self.stderr.write(str(e))
            raise CommandError(e)

        return _('Todas as sincronizações foram executadas!')
