from django.core.management.base import BaseCommand, CommandError
from django.utils.translation import gettext as _
from django.db.transaction import atomic

from team.helper_sync import TeamSync


class Command(BaseCommand):
    """ Sincroniza os dados do Customer com o provedor de pagamentos """

    help = _('Sincronizar os dados dos lotes')
    requires_migrations_checks = True

    @atomic
    def handle(self, *args, **options):

        try:
            sync = TeamSync()
            sync.process_sync()

        except Exception as e:
            raise CommandError(e)

        return _('Corretores sincronizados com sucesso!')
