from rest_framework.serializers import ModelSerializer

from .models import Project, Lot, ProjectFile, ProjectPhoto, Seller, PaymentCondition
from .forms import (
    ProjectForm, LotForm, ProjectFileForm, ProjectPhotoForm, SellerForm,
    PaymentConditionForm
)

from core.serializers.form_serializer_mixin import FormSerializerMixin


class ProjectSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = ProjectForm
        model = Project
        fields = ['uuid', 'active'] + ProjectForm.Meta.fields


class LotSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = LotForm
        model = Lot
        fields = ['project', 'uuid', 'street', 'block', 'name', 'status']


class LotDetailSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = LotForm
        model = Lot
        fields = ['uuid', 'status'] + LotForm.Meta.fields


class ProjectFileSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = ProjectFileForm
        model = ProjectFile
        fields = ['uuid', 'name', 'extension', 'size'] + ProjectFileForm.Meta.fields


class ProjectPhotoSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = ProjectPhotoForm
        model = ProjectPhoto
        fields = ['uuid', 'name', 'extension', 'size'] + ProjectPhotoForm.Meta.fields


class SellerSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = SellerForm
        model = Seller
        fields = ['uuid'] + SellerForm.Meta.fields


class PaymentConditionSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = PaymentConditionForm
        model = PaymentCondition
        fields = ['uuid'] + PaymentConditionForm.Meta.fields
