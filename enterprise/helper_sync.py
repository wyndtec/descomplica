import pytz

from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError

from synchronizer.services.project import ProjectService
from synchronizer.services.lot import LotService
from synchronizer.services.reserved import ReservedService

from core.util.string import clear_string
from customer.choices import MALE, FEMALE
from enterprise.models import Project, Lot
from enterprise.forms import ProjectForm, LotForm
from enterprise.choices import LOT_AVAILABLE, LOT_SELLER, LOT_RESERVED, LOT_PROPOSAL

from commom.sync import HelperSync

USERNAME = settings.USERNAME_SANKHYA
PASSWORD = settings.PASSWORD_SANKHYA
TIMEZONE_SAO_PAULO = pytz.timezone('America/Sao_Paulo')


class ProjectSync(HelperSync):

    def get_all(self):
        """
        Pega todas as informações da Sankhya referente a loteamento
        """
        project = ProjectService()
        project.authenticate(username=USERNAME, password=PASSWORD)
        return project.fetch()

    def convert_sync_to_model(self, data: dict):
        """ Converte os dados vindo do sincronizador para os campos do model """

        return {
            'external_id': data['id'],
            'name': data['name']
        }

    def process_sync(self):
        """ Processa a sincronização """

        # Armazena todos os projetos cadastrados no sistema
        all_projects = Project.objects.filter(external_id__isnull=False)
        # Armazena todos os ids externos
        external_id_projects = [r.external_id for r in all_projects]
        # Armazena todos os dados vindo da Sankhya
        sync_all_data = self.get_all()

        for sync_data in sync_all_data:
            data = self.convert_sync_to_model(sync_data)

            if data['external_id'] in external_id_projects:
                project = [l for l in all_projects if l.external_id == data['external_id']][0]
            else:
                form = ProjectForm(data=data)
                if form.is_valid():
                    project = form.save()
                else:
                    raise ValidationError(form.errors)

            for k, v in data.items():
                setattr(project, k, v)

            project.last_sync = timezone.now()
            project.validate()
            project.save()

        # Apaga todos os registros que não estão mais no servidor da Sankhya
        # Project.objects.exclude(
        #     external_id__in=[d['id'] for d in sync_all_data]
        # ).filter(external_id__isnull=False).delete()


class LotSync(HelperSync):
    def get_all(self, project_external_id):
        """
        Pega todas as informações da Sankhya referente a lotes
        """
        lot_service = LotService()
        lot_service.authenticate(username=USERNAME, password=PASSWORD)
        return lot_service.fetch(project_external_id)

    def convert_sync_to_model(self, data: dict):
        """ Converte os dados vindo do sincronizador para os campos do model

            Status do loteamento na Sankhya

            VALOR   DESCRIÇÃO
            CA      Caução
            DI      Disponível
            DO      Doação
            DS      Distribuído Sócio
            IN      Inativos
            NI      Não Informada
            OU      Outros
            RD      Reserva Diretoria
            RE      Reservado
            RL      Reserva de Liberação
            RM      Remanescente
            RP      Reserva Proprietário
            RS      Reserva Supervisão
            RT      Reserva Técnica
            RV      Reserva de Validação
            VE      Vendido
        """

        status = data['status']

        # TODO: verificar se o status vai ficar assim mesmo
        # if status in ['DI', 'NI', 'RM']:
        #     status = LOT_AVAILABLE
        # elif status in ['RE', 'RD', 'RL', 'RP', 'RS', 'RT', 'RV', 'DS']:
        #     status = LOT_RESERVED
        # elif status in ['CA']:
        #     status = LOT_PROPOSAL
        # elif status in ['DO', 'IN', 'OU', 'VE']:
        #     status = LOT_SELLER
        if status in ['DI']:
            status = LOT_AVAILABLE
        elif status in ['RE', 'RD', 'RL', 'RP', 'RS', 'RT', 'RV']:
            status = LOT_RESERVED
        elif status in ['CA', 'NI', 'RM', 'DS', 'DO', 'IN', 'OU', 'TR', 'RR']:
            status = LOT_PROPOSAL
        elif status in ['VE']:
            status = LOT_SELLER

        return {
            'external_id': data['id'],
            'name': data['name'],
            'status': status,
            'location_latitude': data['latitude'],
            'location_longitude': data['longitude'],
            'street': data['street'],
            'block': data['block'],
            'last_modified_at': data['last_modified_at'],
            'price': data['price'],
            'division_right': data['division_right'],
            'division_left': data['division_left'],
            'division_front': data['division_front'],
            'division_back': data['division_back'],
            'chanfro': data['chanfro'],
            'variant': data['variant'],
            'size': data['size'],
            'size_front': data['size_front'],
            'size_back': data['size_back'],
            'size_right': data['size_right'],
            'size_left': data['size_left'],
            'size_chanfro': data['size_chanfro'],
            'size_variant': data['size_variant'],
            'project__external_id': data['project']
        }

    def process_sync(self):
        """ Processa a sincronização """

        # Armazena todos os lotes cadastrados no sistema já sincronizados
        all_lots = Lot.objects.filter(external_id__isnull=False)

        # Armazena todos os ids externos
        external_id_lots = [r.external_id for r in all_lots]

        for project in Project.objects.filter(external_id__isnull=False).order_by('external_id'):
            # Armazena todos os dados vindo da Sankhya
            sync_all_data = self.get_all(project.external_id)

            for sync_data in sync_all_data:
                data = self.convert_sync_to_model(sync_data)

                if data['external_id'] in external_id_lots:
                    lot = [l for l in all_lots if l.external_id == data['external_id']][0]

                    # Não há dados novos a serem alterados
                    if data['last_modified_at']:
                        if lot.last_sync.astimezone(TIMEZONE_SAO_PAULO) >= data['last_modified_at'].astimezone(TIMEZONE_SAO_PAULO):
                            continue

                else:
                    form = LotForm(data=dict(data, **{'project': project}))
                    if form.is_valid():
                        lot = form.save()
                    else:
                        raise ValidationError(form.errors)

                for k, v in data.items():
                    setattr(lot, k, v)

                lot.last_sync = timezone.now()
                lot.validate()
                lot.save()

            # Apaga todos os registros que não estão mais no servidor da Sankhya
            # Lot.objects.exclude(
            #     external_id__in=[d['id'] for d in sync_all_data]
            # ).filter(external_id__isnull=False, project=project).delete()


class ReservedSync(HelperSync):
    """ Sincroniza a reserva de um lote """

    def convert_sync_to_model(self, data: dict):
        """ Converte os dados vindo do sincronizador para os campos do model """

        documents = []
        phones = []
        emails = []

        for document in data['documents']:
            documents.append({
                'doc_type': document['type'],
                'doc_number': document['number']
            })

        for phone in data['phones']:
            p = clear_string(phone)
            if len(p) >= 11:
                phones.append({
                    'ddi': '55',
                    'ddd': p[:2],
                    'number': p[2:]
                })

        for email in data.get('emails'):
            if email:
                emails.append(email)

        return {
            'external_id': data['id'],
            'reserved_date': data['reserved_date'],
            'reserved_start': data['reserved_start'],
            'status': data['status'],
            'seller__external_id': data['seller'],
            'lot': {
                'external_id': data['lot'],
                'name': data['lot_name']
            },
            'prospect': {
                'gender': MALE if data['gender'] == 'M' else FEMALE,
                'name': data['name'],
                'external_id': data['prospect'],
                'documents': documents,
                'phones': phones,
                'emails': emails
            },
        }

    def process_sync(self, attendance):
        """ Criar uma reserva no WS com os dados do atendimento """

        reserved = ReservedService()
        reserved.authenticate(username=USERNAME, password=PASSWORD)

        customer = attendance.customer
        lot = attendance.lot
        document = customer.documents.filter(doc_type__in=['cpf', ' cnpj']).first()
        email = customer.emails.all().first()
        phone = customer.phones.all().first()
        phone_number = f'{phone.ddi}{phone.ddd}{phone.number}'

        sync_response = reserved.save(
                lot=lot.external_id,
                type_person='F' if customer.is_person else 'J',
                cpf_cnpj=document.doc_number,
                gender='H' if customer.gender == MALE else 'M',
                name=customer.get_full_name,
                email=email.email if email else None,
                phone=phone_number,
                seller=attendance.seller.member.external_id
            )

        response = self.convert_sync_to_model(sync_response[0])

        customer.external_id = response['prospect']['external_id']
        customer.save(ignore_validation=True)

        return response
