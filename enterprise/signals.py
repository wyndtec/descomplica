

def project_status(sender, instance, created, *args, **kwargs):
    """" Signal que controla o status do projeto baseado nos lotes do loteamento """

    from enterprise.models import Lot, Project
    from enterprise.choices import LOT_SELLER

    lot = instance

    qs_active_lots = Lot.objects.exclude(status=LOT_SELLER).filter(project=lot.project)
    lot.project.active = qs_active_lots.exists()
    lot.project.save(ignore_validation=True)
