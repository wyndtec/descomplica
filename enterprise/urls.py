from rest_framework_extensions.routers import ExtendedSimpleRouter

from .views import (
    ProjectViewSet, LotViewSet, ProjectFileViewSet, ProjectPhotoViewSet,
    SellerViewSet, PaymentConditionViewSet
)

app_name = 'project'

router = ExtendedSimpleRouter()
router_project = router.register('projects', ProjectViewSet, 'project')
router_project.register('lots', LotViewSet, 'lot', parents_query_lookups=['project'])
router_project.register('files', ProjectFileViewSet, 'file', parents_query_lookups=['project'])
router_project.register('photos', ProjectPhotoViewSet, 'file', parents_query_lookups=['project'])
# TODO: descomentar quando tiver pronto a gestão de equipes
# router_project.register('sellers', SellerViewSet, 'seller', parents_query_lookups=['project'])
router.register('sellers', SellerViewSet, 'seller')
router_project.register('paymentcondictions', PaymentConditionViewSet, 'paymancondition',
                        parents_query_lookups=['project'])

urlpatterns = router.urls
