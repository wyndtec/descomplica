from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import FileExtensionValidator, get_available_image_extensions

from core.models.mixins import ActivableMixin

from commom.models import AbstractModel, AbstractSyncModel
from enterprise.choices import LOT_STATUS, LOT_AVAILABLE
from team.models import TeamMember


class Project(AbstractSyncModel):
    name = models.CharField(max_length=50, help_text=_('Nome do empreendimento'))
    active = models.BooleanField(verbose_name=_('Ativo'), default=True, editable=False)

    def __str__(self):
        return self.name

    class Meta(AbstractModel.Meta):
        verbose_name = _('Empreendimento')


class Lot(AbstractSyncModel):
    project = models.ForeignKey(Project, on_delete=models.PROTECT,
                                help_text=_('Empreendimento'), related_name='lots')
    name = models.CharField(max_length=50, help_text=_('Nome do lote'))
    status = models.CharField(max_length=12, choices=LOT_STATUS, default=LOT_AVAILABLE,
                              help_text=_('Estado atual do lote'), editable=False)
    location_latitude = models.FloatField(help_text=_('Coordenada de latitude do lote'))
    location_longitude = models.FloatField(help_text=_('Coordenada de longitude do lote'))
    street = models.CharField(max_length=50, help_text=_('Rua do lote'), null=True, blank=True)
    block = models.CharField(max_length=50, help_text=_('Quadra do lote'), null=True, blank=True)
    price = models.FloatField(help_text=_('Valor do lote'), null=True, blank=True)
    division_right = models.CharField(max_length=50, help_text=_('Divisão da direita do lote'), null=True, blank=True)
    division_left = models.CharField(max_length=50, help_text=_('Divisão da esquerda do lote'), null=True, blank=True)
    division_front = models.CharField(max_length=50, help_text=_('Divisão de frente do lote'), null=True, blank=True)
    division_back = models.CharField(max_length=50, help_text=_('Divisão de trás do lote'), null=True, blank=True)
    chanfro = models.CharField(max_length=50, help_text=_('Chanfro do lote'), null=True, blank=True)
    variant = models.CharField(max_length=50, help_text=_('Variação da metragem do lote'), null=True, blank=True)
    size = models.FloatField(help_text=_('Área do lote'))
    size_front = models.CharField(max_length=50, help_text=_('Dimensão de frente do lote'), null=True, blank=True)
    size_back = models.CharField(max_length=50, help_text=_('Dimensão de trás do lote'), null=True, blank=True)
    size_right = models.CharField(max_length=50, help_text=_('Dimensão da direita do lote'), null=True, blank=True)
    size_left = models.CharField(max_length=50, help_text=_('Dimensão da esquerda do lote'), null=True, blank=True)
    size_chanfro = models.CharField(max_length=50, help_text=_('Tamanho do chanfro'), null=True, blank=True)
    size_variant = models.CharField(max_length=50, help_text=_('Tamanho da variação'), null=True, blank=True)

    def __str__(self):
        return f'{self.project} | {self.name}'

    class Meta(AbstractModel.Meta):
        verbose_name = _('Lote')


class ProjectFile(AbstractModel, ActivableMixin):
    project = models.ForeignKey(Project, on_delete=models.PROTECT,
                                help_text=_('Empreendimento'), related_name='files')
    name = models.CharField(max_length=150, help_text=_('Nome do arquivo'), editable=False)
    extension = models.CharField(max_length=10, help_text=_('Extensão do arquivo'), editable=False)
    size = models.IntegerField(help_text=_('Tamanho do arquivo'), editable=False)
    file = models.FileField(help_text=_('Arquivo'), validators=[FileExtensionValidator(
        allowed_extensions=['pdf'] + get_available_image_extensions()
    )], upload_to='enterprise/files')

    def __str__(self):
        return f'{self.project} | {self.name}'

    class Meta(AbstractModel.Meta):
        verbose_name = _('Arquivo do empreendimento')
        verbose_name_plural = _('Arquivos do empreendimento')


class ProjectPhoto(AbstractModel, ActivableMixin):
    project = models.ForeignKey(Project, on_delete=models.PROTECT,
                                help_text=_('Empreendimento'), related_name='photos')
    name = models.CharField(max_length=150, help_text=_('Nome da imagem'), editable=False)
    extension = models.CharField(max_length=10, help_text=_('Extensão da imagem'), editable=False)
    size = models.IntegerField(help_text=_('Tamanho do arquivo'), editable=False)
    file = models.ImageField(help_text=_('Imagem'), upload_to='enterprise/photos')

    def __str__(self):
        return f'{self.project} | {self.name}'

    class Meta(AbstractModel.Meta):
        verbose_name = _('Imagem do empreendimento')
        verbose_name_plural = _('Imagens do empreendimento')


class Seller(AbstractModel, ActivableMixin):
    # TODO: temporariamente está como não obrigatório, posteriormente vai ser
    # alterado para obrigatório assim que existir a gestão de equipes, pois o
    # vendedor precisa obrigatoriamente ligado a um project
    project = models.ForeignKey(Project, on_delete=models.PROTECT, related_name='sellers',
                                help_text=_('Empreendimento'), null=True, blank=True)
    member = models.ForeignKey(TeamMember, on_delete=models.PROTECT, )

    def __str__(self):
        return _(f'Vendedor {self.member}')

    class Meta(AbstractModel.Meta):
        verbose_name = _('Vendedor')
        verbose_name_plural = _('Vendedores')


class PaymentCondition(AbstractModel, ActivableMixin):
    project = models.ForeignKey(Project, on_delete=models.PROTECT, related_name='payment_condictions',
                                help_text=_('Empreendimento'))
    min_parts = models.IntegerField(help_text=_('Mínimo de parcelas'))
    max_parts = models.IntegerField(help_text=_('Máximo de parcelas'))
    interest = models.FloatField(help_text=_('Percentual de juros'))
    fine_per_day = models.FloatField(help_text=_('Percentual de multa por dia de atraso'))
    table_name = models.CharField(max_length=50, help_text=_('Nome da tabela'))

    def __str__(self):
        return f'{self.project} | {self.table_name}'

    class Meta(AbstractModel.Meta):
        verbose_name = _('Condição de pagamento')
        verbose_name_plural = _('Condições de pagamento')
