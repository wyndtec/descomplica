from django.apps import AppConfig

from django.db.models.signals import post_save


class EnterpriseConfig(AppConfig):
    name = 'enterprise'

    def ready(self):
        from .signals import project_status
        post_save.connect(project_status, sender='enterprise.Lot')
