from django.contrib import admin

from .models import (
    Project, Lot, ProjectFile, ProjectPhoto, Seller, PaymentCondition
)

from .forms import (
    ProjectForm, LotForm, ProjectFileForm, ProjectPhotoForm, SellerForm,
    PaymentConditionForm
)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'created_at', 'updated_at', 'name', 'active')
    list_filter = ('created_at', 'updated_at', 'active')
    search_fields = ('name',)
    date_hierarchy = 'created_at'
    form = ProjectForm


@admin.register(Lot)
class LotAdmin(admin.ModelAdmin):
    list_display = (
        'uuid',
        'created_at',
        'updated_at',
        'external_id',
        'last_sync',
        'project',
        'name',
        'status',
        'location_latitude',
        'location_longitude',
        'street',
        'block',
        'price',
        'division_right',
        'division_left',
        'division_front',
        'division_back',
        'chanfro',
        'variant',
        'size_front',
        'size_back',
        'size_right',
        'size_left',
        'size_chanfro',
        'size_variant',
    )
    list_filter = ('created_at', 'updated_at', 'last_sync', 'project')
    search_fields = ('name',)
    date_hierarchy = 'created_at'
    form = LotForm


@admin.register(ProjectFile)
class ProjectFileAdmin(admin.ModelAdmin):
    list_display = (
        'active',
        'uuid',
        'created_at',
        'updated_at',
        'project',
        'name',
        'extension',
        'size',
        'file',
    )
    list_filter = ('active', 'created_at', 'updated_at', 'project')
    search_fields = ('name',)
    date_hierarchy = 'created_at'
    form = ProjectFileForm


@admin.register(ProjectPhoto)
class ProjectPhotoAdmin(admin.ModelAdmin):
    list_display = (
        'active',
        'uuid',
        'created_at',
        'updated_at',
        'project',
        'name',
        'extension',
        'size',
        'file',
    )
    list_filter = ('active', 'created_at', 'updated_at', 'project')
    search_fields = ('name',)
    date_hierarchy = 'created_at'
    form = ProjectPhotoForm


@admin.register(Seller)
class SellerAdmin(admin.ModelAdmin):
    list_display = (
        'active',
        'uuid',
        'created_at',
        'updated_at',
        'project',
        'member',
    )
    list_filter = ('active', 'created_at', 'updated_at', 'project', 'member')
    date_hierarchy = 'created_at'
    form = SellerForm


@admin.register(PaymentCondition)
class PaymentConditionAdmin(admin.ModelAdmin):
    list_display = (
        'active',
        'uuid',
        'created_at',
        'updated_at',
        'project',
        'min_parts',
        'max_parts',
        'interest',
        'fine_per_day',
        'table_name',
    )
    list_filter = ('active', 'created_at', 'updated_at', 'project')
    date_hierarchy = 'created_at'
    form = PaymentConditionForm
