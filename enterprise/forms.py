import os

from django import forms

from .models import Project, Lot, ProjectFile, ProjectPhoto, Seller, PaymentCondition


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name']


class LotForm(forms.ModelForm):
    class Meta:
        model = Lot
        fields = ['project', 'name', 'location_latitude', 'location_longitude',
                  'street', 'block', 'price', 'division_right', 'division_left',
                  'division_front', 'division_back', 'chanfro', 'variant', 'size',
                  'size_front', 'size_back', 'size_right', 'size_left',
                  'size_chanfro', 'size_variant']


class ProjectFileForm(forms.ModelForm):

    def clean_file(self):
        file = self.cleaned_data.get('file')

        if file:
            self.instance.name = file.name
            self.instance.size = file.size
            _, self.instance.extension = os.path.splitext(file.name)
        return file

    class Meta:
        model = ProjectFile
        fields = ['project', 'file', 'active']


class ProjectPhotoForm(forms.ModelForm):

    def clean_file(self):
        file = self.cleaned_data.get('file')

        if file:
            self.instance.name = file.name
            self.instance.size = file.size
            _, self.instance.extension = os.path.splitext(file.name)
        return file

    class Meta:
        model = ProjectPhoto
        fields = ['project', 'file', 'active']


class SellerForm(forms.ModelForm):

    class Meta:
        model = Seller
        fields = ['project', 'member', 'active']


class PaymentConditionForm(forms.ModelForm):

    class Meta:
        model = PaymentCondition
        fields = ['project', 'min_parts', 'max_parts', 'interest',
                  'fine_per_day', 'table_name', 'active']
