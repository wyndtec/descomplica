from djangorestframework_camel_case.parser import CamelCaseMultiPartParser, CamelCaseJSONParser

from commom.viewset import PermissionModelViewSet

from .models import Project, Lot, ProjectFile, ProjectPhoto, Seller, PaymentCondition
from .serializers import (
    ProjectSerializer, LotSerializer, ProjectFileSerializer, ProjectPhotoSerializer,
    SellerSerializer, PaymentConditionSerializer, LotDetailSerializer
)

from rest_framework.filters import SearchFilter
from rest_framework.decorators import action
from rest_framework.response import Response

from .choices import LOT_AVAILABLE, LOT_SELLER

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework_extensions.mixins import NestedViewSetMixin, DetailSerializerMixin


class ProjectViewSet(PermissionModelViewSet):
    queryset = Project.objects.filter()
    serializer_class = ProjectSerializer
    filter_backends = [SearchFilter]
    search_fields = ['name']

    @action(methods=['get'], url_path='stats', detail=False)
    def stats(self, request):
        project = Project.objects.count()
        lot_avaiable = Lot.objects.filter(status=LOT_AVAILABLE).count()
        lot_sellers = Lot.objects.filter(status=LOT_SELLER).count()

        return Response(
            data={
                'project': project,
                'lot_avaiable': lot_avaiable,
                'lot_sellers': lot_sellers,
            }
        )


class LotViewSet(NestedViewSetMixin, DetailSerializerMixin, PermissionModelViewSet):
    queryset = Lot.objects.filter()
    serializer_class = LotSerializer
    serializer_detail_class = LotDetailSerializer
    filter_backends = [SearchFilter, DjangoFilterBackend]
    search_fields = ['name']
    filterset_fields = ['status', 'name', 'street', 'block']


class ProjectFileViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = ProjectFile.objects.filter()
    serializer_class = ProjectFileSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['active', 'name']
    parser_classes = [CamelCaseMultiPartParser, CamelCaseJSONParser]


class ProjectPhotoViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = ProjectPhoto.objects.filter()
    serializer_class = ProjectPhotoSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['active', 'name']
    parser_classes = [CamelCaseMultiPartParser, CamelCaseJSONParser]


# TODO: descomentar quando tiver pronto a gestão de equipes
# class SellerViewSet(NestedViewSetMixin, PermissionModelViewSet):
class SellerViewSet(PermissionModelViewSet):
    queryset = Seller.objects.filter()
    serializer_class = SellerSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['active', 'member']


class PaymentConditionViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = PaymentCondition.objects.filter()
    serializer_class = PaymentConditionSerializer
    filter_backends = [SearchFilter, DjangoFilterBackend]
    filterset_fields = ['active']
    search_fields = ['table_name']
