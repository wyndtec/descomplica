from django.utils.translation import gettext as _

LOT_AVAILABLE = 'available'
LOT_RESERVED = 'reserved'
LOT_PROPOSAL = 'proposal'
LOT_SELLER = 'seller'

LOT_STATUS = (
    (LOT_AVAILABLE, _('Disponível')),
    (LOT_RESERVED, _('Reservado')),
    (LOT_PROPOSAL, _('Com proposta')),
    (LOT_SELLER, _('Vendido')),
)
