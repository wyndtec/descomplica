"""descomplica_vendas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from rest_framework.authtoken.views import ObtainAuthToken


#TODO: remover assim que for criado o sistema de autenticação
class ObtainAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        from rest_framework.authtoken.models import Token
        from rest_framework.response import Response

        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        try:
            member = str(user.members.pk)
        except Exception:
            member = None

        return Response({
            'token': token.key,
            'user_id': user.pk,
            'email': user.email,
            'member': member
        })

urlpatterns = [
    path('admin/', admin.site.urls),
    path('project/', include('enterprise.urls')),
    path('customer/', include('customer.urls')),
    path('attendance/', include('attendance.urls')),
    path('team/', include('team.urls')),

    #TODO: remover assim que for criado o sistema de autenticação
    # path('api-token-auth/', auth_views.obtain_auth_token)
    path('api-token-auth/', ObtainAuthToken.as_view())
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
