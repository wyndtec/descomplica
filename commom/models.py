from itertools import chain

from django.core.validators import MinLengthValidator
from django.db import models
from django.utils.translation import gettext as _

from localflavor.br.models import BRStateField

from core.models.mixins import (UUIDPkMixin, EntityMixin, DeletableModelMixin,
                                DomainRuleMixin)

from .choices import DOC_TYPES, DOC_TYPES_PF, DOC_TYPES_PJ
from .rules import ContactValidated, DocumentValidated


class AbstractModel(UUIDPkMixin, EntityMixin, DeletableModelMixin,
                    DomainRuleMixin, models.Model):
    """
    Model para ser utilizado como base para todos os demais models do sistema
    """

    created_at = models.DateTimeField(
        verbose_name=_('created at'),
        auto_now_add=True,
        null=False,
        blank=False,
        editable=False,
    )
    updated_at = models.DateTimeField(
        verbose_name=_('updated at'),
        auto_now=True,
        null=False,
        blank=False,
        editable=False,
    )

    def to_dict(self):
        """ Transaforma o model em um dict """

        opts = self._meta
        data = {}
        for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
            data[f.name] = f.value_from_object(self)
        return data

    def __iter__(self):
        data = self.to_dict()
        for k in data:
            yield k, data[k]

    class Meta:
        abstract = True
        ordering = ['-created_at']


class AbstractSyncModel(AbstractModel):
    """
    Model base para ser utilizado nos models que precisam ser sincronizados com
    As API do Zoop
    """

    external_id = models.IntegerField(
        verbose_name=_('ID que identifica o registro na Sankhya'),
        unique=True,
        null=True,
        blank=True,
        editable=False
    )
    last_sync = models.DateTimeField(
        verbose_name=_('última sincronização'),
        null=True,
        editable=False,
    )

    class Meta:
        abstract = True


class DocumentQueryset(models.QuerySet):
    def only_company(self):
        """ Filtra para retornar apenas registros de Empresas """

        document = self.model.documents.rel.related_model
        field_name = self.model.documents.rel.field.attname
        return self.filter(
            models.Exists(
                document.objects.filter(
                    **{field_name: models.OuterRef('pk')},
                    doc_type__in=[f[0] for f in DOC_TYPES_PJ]
                ).values('pk'),
            )
        )

    def only_person(self):
        """ Filtrar para retornar apenas registros de Pessoas Físicas """

        document = self.model.documents.rel.related_model
        field_name = self.model.documents.rel.field.attname
        return self.filter(
            models.Exists(
                document.objects.filter(
                    **{field_name: models.OuterRef('pk')},
                    doc_type__in=[f[0] for f in DOC_TYPES_PF],
                ).values('pk'),
            )
        )


class DocumentManager(models.Manager):
    """ Manager para ser usado nos objetos principais que usam documents """

    def get_queryset(self):
        self._queryset_class = DocumentQueryset
        return super(DocumentManager, self).get_queryset()


class AbstractDocument(models.Model):
    """
    Model abstrato para servir como base para as classes que lidam com Document
    """

    doc_type = models.CharField(
        choices=DOC_TYPES, max_length=7, help_text=_('Tipo de documento')
    )
    doc_number = models.CharField(max_length=14, validators=[
        MinLengthValidator(limit_value=7, message=_('Número do documento inválido'))]
    )

    class Meta(AbstractModel.Meta):
        abstract = True

    integrity_rules = [
        DocumentValidated
    ]

    def __str__(self):
        return f'{self.doc_type} | {self.doc_number}'


class AbstractContact(AbstractModel):
    """
    Model abstrato para servir como base para as classes que lidam com dados de contato
    """

    description = models.CharField(max_length=50, help_text=_('Descrição do contato'))
    email = models.EmailField(help_text=_('Endereço de e-mail'), null=True, blank=True)
    phone_number = models.CharField(
        max_length=11, help_text=_('Telefone de contato'), validators=[
        MinLengthValidator(limit_value=10, message=_('Número de telefone inválido'))],
        null=True, blank=True
    )
    street = models.CharField(max_length=60, help_text=_('Logradouro/Rua'))
    address_number = models.CharField(max_length=15, help_text=_('Número'), null=True, blank=True)
    neighborhood = models.CharField(max_length=60, help_text=_('Bairro'))
    address_complement = models.CharField(max_length=60, help_text=_('Complemento'), null=True, blank=True)
    city = models.CharField(max_length=60, help_text=_('Cidade'))
    address_state = BRStateField(help_text=_('Estado'))
    postal_code = models.CharField(help_text=_('CEP'), max_length=8)

    integrity_rules = [
        ContactValidated
    ]

    class Meta(AbstractModel.Meta):
        abstract = True

    def __str__(self):
        return self.description
