from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from .permission import ModelPermissions


class PermissionModelViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated, ModelPermissions]
