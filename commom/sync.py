class HelperSync:

    def get_all(self):
        """
        Pega todas as informações da Sankhya
        """
        raise NotImplemented

    def convert_sync_to_model(self, data: dict) -> dict:
        """ Converte os dados vindo do sincronizador para os campos do model """
        raise NotImplemented

    def process_sync(self):
        """ Processa a sincronização """
        raise NotImplemented
