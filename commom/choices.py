from django.utils.translation import gettext as _

DOC_TYPES_PF = (
    ('cpf', _('Número do CPF')),
    ('rg', _('RG')),
)

DOC_TYPES_PJ = (
    ('cnpj', _('CNPJ')),
    ('ie', _('Inscrição Estadual')),
    ('im', _('Inscrição Municipal')),
)

OTHER_DOC_TYPE = (
    ('address', _('Comprovante de endereço')),
)

# Documentos dos corretores
BROKERS_DOC = (
    ('creci', _('Conselho Regional de Corretores de Imóveis')),
)

DOC_TYPES = DOC_TYPES_PF + DOC_TYPES_PJ + OTHER_DOC_TYPE
