from django import forms
from django.utils.translation import gettext as _
from django.core.exceptions import ValidationError

from localflavor.br.validators import BRCPFValidator, BRCNPJValidator

from commom.choices import DOC_TYPES_PJ, DOC_TYPES_PF


class DocumentForm(forms.ModelForm):

    def get_all_documents(self):
        """ Retornar os dados de todos os documentos """
        raise NotImplemented

    def get_owner_documents(self):
        """ Retorna uma instância do dono do documentos """
        raise NotImplemented

    def clean_doc_number(self):
        doc_number = self.cleaned_data.get('doc_number')
        doc_type = self.cleaned_data.get('doc_type')
        person = self.get_owner_documents()

        if not (person and doc_number and doc_type):
            return doc_number

        documents = self.get_all_documents()

        pj_doc = [f[0] for f in DOC_TYPES_PJ]
        pf_doc = [f[0] for f in DOC_TYPES_PF]
        person_pf = any([d.doc_type in pf_doc for d in documents])
        person_pj = any([d.doc_type in pj_doc for d in documents])

        if doc_type == 'cpf':
            BRCPFValidator()(doc_number)
        elif doc_type == 'cnpj':
            BRCNPJValidator()(doc_number)

        if person_pj and doc_type in pf_doc:
            raise ValidationError(
                _('Um PJ não pode possuir esse tipo de documento')
            )

        if person_pf and doc_type in pj_doc:
            raise ValidationError(
                _('Um PF não pode possuir esse tipo de documento')
            )

        return doc_number
