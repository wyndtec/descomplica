from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from localflavor.br.validators import BRCPFValidator, BRCNPJValidator

from core.models.mixins import IntegrityRuleChecker, RuleIntegrityError

from .choices import DOC_TYPES_PF, DOC_TYPES_PJ


class DocumentValidated(IntegrityRuleChecker):
    """ Valida se foi informado um documento cpf ou cnpj válido """

    def check(self, instance) -> None:
        pj_doc = [f[0] for f in DOC_TYPES_PJ]
        pf_doc = [f[0] for f in DOC_TYPES_PF]

        try:
            if instance.doc_type == 'cpf':
                BRCPFValidator()(instance.doc_number)
            elif instance.doc_type == 'cnpj':
                BRCNPJValidator()(instance.doc_number)
        except ValidationError as e:
            raise RuleIntegrityError(
                _(e.message),
                field_name='doc_number'
            )

        if instance.customer.is_company and instance.doc_type in pf_doc:
            raise RuleIntegrityError(
                _('Um PJ não pode possuir esse tipo de documento'),
                field_name='doc_type'
            )

        if instance.customer.is_person and instance.doc_type in pj_doc:
            raise RuleIntegrityError(
                _('Um PF não pode possuir esse tipo de documento'),
                field_name='doc_type'
            )


class ContactValidated(IntegrityRuleChecker):
    """ Valida se foi informado algum dado de contato """

    def check(self, instance) -> None:
        if instance.city and not instance.address_state:
            raise RuleIntegrityError(
                message=_('Não foi informado o estado da cidade'),
                field_name='address_state'
            )

        if instance.address_state and not instance.city:
            raise RuleIntegrityError(
                message=_('Não foi informado a cidade do estado'),
                field_name='city'
            )

        if (instance.address_state or instance.city) and not instance.street:
            raise RuleIntegrityError(
                message=_('Não foi informado o endereço'),
                field_name='street'
            )
