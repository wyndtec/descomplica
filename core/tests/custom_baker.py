from model_bakery.baker import Baker

from django.db.models.fields.related import ReverseManyToOneDescriptor


class CustomBaker(Baker):

    def instance(self, attrs, _commit, _save_kwargs, _from_manager):
        one_to_many_keys = {}
        for k in tuple(attrs.keys()):
            field = getattr(self.model, k, None)
            if isinstance(field, ReverseManyToOneDescriptor):
                one_to_many_keys[k] = attrs.pop(k)

        instance = self.model(**attrs)
        # m2m only works for persisted instances
        if _commit:
            if hasattr(instance, 'validate'):
                instance.validate()
            instance.save(**_save_kwargs)
            self._handle_one_to_many(instance, one_to_many_keys)
            self._handle_m2m(instance)

            if _from_manager:
                # Fetch the instance using the given Manager, e.g.
                # 'objects'. This will ensure any additional code
                # within its get_queryset() method (e.g. annotations)
                # is run.
                manager = getattr(self.model, _from_manager)
                instance = manager.get(pk=instance.pk)

        return instance
