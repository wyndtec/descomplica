from rest_framework.serializers import ModelSerializer, ManyRelatedField, PrimaryKeyRelatedField, StringRelatedField

from .models import Customer, CustomerDocument, Phone, CustomerEmail
from .forms import CustomerForm, CustomerDocumentForm, PhoneForm, CustomerEmailForm

from core.serializers.form_serializer_mixin import FormSerializerMixin


class CustomerDocumentSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = CustomerDocumentForm
        model = CustomerDocument
        fields = ['uuid'] + CustomerDocumentForm.Meta.fields


class PhoneSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = PhoneForm
        model = Phone
        fields = ['uuid'] + PhoneForm.Meta.fields


class CustomerEmailSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = CustomerEmailForm
        model = CustomerEmail
        fields = ['uuid'] + CustomerEmailForm.Meta.fields


class CustomerSerializer(FormSerializerMixin, ModelSerializer):

    # documents = CustomerDocumentSerializer(many=True)
    # phones = PhoneSerializer(many=True)
    # emails = CustomerEmailSerializer(many=True)

    class Meta:
        form = CustomerForm
        model = Customer
        # fields = ['uuid', 'documents', 'phones', 'emails'] + CustomerForm.Meta.fields
        fields = ['uuid'] + CustomerForm.Meta.fields
