from django.db.transaction import atomic
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError

from synchronizer.services.prospect import ProspectService as CustomerService
from synchronizer.services.partner import PartnerService

from core.util.string import clear_string
from customer.forms import CustomerForm, CustomerDocumentForm, PhoneForm, CustomerEmailForm
from customer.choices import SYNC_ORIGIN

from .choices import MALE, FEMALE

from commom.sync import HelperSync

USERNAME = settings.USERNAME_SANKHYA
PASSWORD = settings.PASSWORD_SANKHYA


class CustomerSync(HelperSync):

    @classmethod
    def get_prospect(cls, cpf_cnpj: str) -> dict:
        """
        Pega todas as informações da Sankhya referente ao prospect/cliente
        :param cpf_cnpj: cpf_cnpj a ser consultado
        """
        project = CustomerService()
        project.authenticate(username=USERNAME, password=PASSWORD)
        return project.fetch(cpf_cnpj)

    @classmethod
    def get_partner(cls, cpf_cnpj: str) -> dict:
        """
        Pega as informações da Sankhya referente ao parceiro/cliente
        :param cpf_cnpj: cpf_cnpj a ser consultado
        """
        partner = PartnerService()
        partner.authenticate(username=USERNAME, password=PASSWORD)
        return partner.fetch(cpf_cnpj)

    @classmethod
    def convert_sync_to_model(cls, data: dict) -> dict:
        """ Converte os dados vindo do sincronizador para os campos do model """

        name_split = data['name'].split()
        first_name = name_split[0]
        last_name = ' '.join(name_split[1:])

        documents = []
        phones = []
        emails = []

        for document in data['documents']:
            documents.append({
                'doc_type': document['type'],
                'doc_number': document['number'],
            })

        for phone in data['phones']:
            p = clear_string(phone)
            if len(p) >= 11:
                phones.append({
                    'ddi': '55',
                    'ddd': p[:2],
                    'number': p[2:]
                })

        for email in data.get('emails'):
            if email:
                emails.append(email)

        return {
            'external_id': data['id'],
            'first_name': first_name,
            'last_name': last_name,
            'gender': MALE if data['gender'] == 'M' else FEMALE,
            'documents': documents,
            'phones': phones,
            'emails': emails
        }

    @classmethod
    @atomic
    def save_model(cls, data):
        form = CustomerForm(data=data)
        if form.is_valid():
            customer = form.save()
            customer.origin = SYNC_ORIGIN
            customer.last_sync = timezone.now()
            if data['external_id']:
                customer.external_id = data['external_id']
            customer.save(ignore_validation=True)
        else:
            raise ValidationError(form.errors)

        for document in data['documents']:
            form = CustomerDocumentForm(data=dict(document, **{'customer': customer}))
            if form.is_valid():
                form.save()
            else:
                raise ValidationError(form.errors)

        for phone in data['phones']:
            form = PhoneForm(data=dict(phone, **{'customer': customer}))
            if form.is_valid():
                form.save()
            else:
                raise ValidationError(form.errors)

        for email in data['emails']:
            form = CustomerEmailForm(data={'customer': customer, 'email': email})
            if form.is_valid():
                form.save()
            else:
                raise ValidationError(form.errors)
