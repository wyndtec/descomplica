from django.contrib import admin

from .models import Customer, CustomerDocument, Phone, CustomerEmail
from .forms import CustomerForm, CustomerDocumentForm, PhoneForm, CustomerEmailForm


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'uuid',
        'created_at',
        'updated_at',
        'first_name',
        'last_name',
        'get_full_name',
        'user',
    )
    list_filter = ('created_at', 'updated_at', 'user')
    date_hierarchy = 'created_at'
    form = CustomerForm


@admin.register(CustomerDocument)
class CustomerDocumentAdmin(admin.ModelAdmin):
    list_display = (
        'uuid',
        'created_at',
        'updated_at',
        'doc_type',
        'doc_number',
        'customer',
        'file',
    )
    list_filter = ('created_at', 'updated_at', 'customer')
    date_hierarchy = 'created_at'
    form = CustomerDocumentForm

@admin.register(Phone)
class PhoneAdmin(admin.ModelAdmin):
    list_display = (
        'uuid',
        'created_at',
        'updated_at',
        'customer',
        'ddi',
        'ddd',
        'number',
    )
    list_filter = ('created_at', 'updated_at', 'customer')
    date_hierarchy = 'created_at'
    form = PhoneForm


@admin.register(CustomerEmail)
class CustomerEmailAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'created_at', 'updated_at', 'customer', 'email')
    list_filter = ('created_at', 'updated_at', 'customer')
    date_hierarchy = 'created_at'
    form = CustomerEmailForm
