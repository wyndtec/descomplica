from rest_framework_extensions.mixins import NestedViewSetMixin

from commom.viewset import PermissionModelViewSet

from .helper_sync import CustomerSync
from .models import Customer, CustomerDocument, Phone, CustomerEmail
from .serializers import (
    CustomerSerializer, CustomerDocumentSerializer, PhoneSerializer,
    CustomerEmailSerializer
)

from rest_framework.filters import SearchFilter

from django_filters.rest_framework import DjangoFilterBackend
from django_filters import FilterSet, CharFilter, UUIDFilter
from django.db.models import Q
from django.utils.translation import gettext as _

from django.core.exceptions import ValidationError

from localflavor.br.validators import BRCPFValidator, BRCNPJValidator


class CustomerFilter(FilterSet):
    cpf_cnpj = CharFilter(field_name='documents', lookup_expr='doc_number',
                          label='cpf/cnpj', method='filter_cpf_cnpj')
    project = UUIDFilter(field_name='attendances__lot__project', label=_('Empreendimento'))

    def filter_cpf_cnpj(self, queryset, field_name, value):

        # Validando se o cpf/cnpj é válido
        try:
            if len(value) <= 11:
                BRCPFValidator()(value)
            else:
                BRCNPJValidator()(value)
        except ValidationError:
            return queryset.none()

        # Verifica se existe um customer com esse cnpj na base
        filter = Q(documents__doc_type__in=['cpf', 'cnpj'], documents__doc_number=value)

        if queryset.filter(filter).exists():
            return queryset.filter(filter)

        # partner = CustomerSync.get_partner(value)
        # if partner:
        #     #TODO: pego o primeiro registro, isso pode gerar erro, olhar futuramente
        #     CustomerSync.save_model(CustomerSync.convert_sync_to_model(partner[0]))
        # else:

        prospect = CustomerSync.get_prospect(value)
        if prospect:
            CustomerSync.save_model(CustomerSync.convert_sync_to_model(prospect[0]))

        return queryset.filter(filter)

    class Meta:
        model = Customer
        fields = ['cpf_cnpj']


class CustomerViewSet(PermissionModelViewSet):
    queryset = Customer.objects.filter()
    serializer_class = CustomerSerializer
    filter_backends = [SearchFilter, DjangoFilterBackend]
    search_fields = ['first_name', 'last_name']
    filterset_class = CustomerFilter


class CustomerDocumentViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = CustomerDocument.objects.filter()
    serializer_class = CustomerDocumentSerializer


class PhoneViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = Phone.objects.filter()
    serializer_class = PhoneSerializer


class CustomerEmailViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = CustomerEmail.objects.filter()
    serializer_class = CustomerEmailSerializer
