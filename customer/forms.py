from django import forms
from django.utils.translation import gettext as _
from django.core.exceptions import ValidationError

from commom.forms import DocumentForm
from commom.choices import DOC_TYPES_PJ

from .models import Customer, CustomerDocument, Phone, CustomerEmail


class CustomerForm(forms.ModelForm):

    class Meta:
        model = Customer
        fields = ['first_name', 'last_name', 'user', 'gender']


class CustomerDocumentForm(DocumentForm):

    def get_all_documents(self):
        customer = self.cleaned_data.get('customer')
        return customer.documents.all()

    def get_owner_documents(self):
        return self.cleaned_data.get('customer')

    def clean_customer(self):
        customer = self.cleaned_data.get('customer')
        if 'customer' in self.changed_data and not self.instance.is_new:
            if self.instance.customer != customer:
                raise ValidationError(
                    _('Não é possível alterar o cliente dono do documento')
                )

        return customer

    def clean_doc_type(self):
        doc_type = self.cleaned_data.get('doc_type')

        if doc_type in DOC_TYPES_PJ:
            raise ValidationError(
                    _('Cadastro de pessoa jurídica está desabilitado!')
                )

        return doc_type

    class Meta:
        model = CustomerDocument
        fields = ['customer', 'doc_type', 'doc_number', 'file']


class PhoneForm(forms.ModelForm):

    def clean_ddi(self):
        ddi = self.cleaned_data.get('ddi')

        if not ddi.isnumeric():
            raise ValidationError(_('DDI inválido'))

        return ddi

    def clean_ddd(self):

        ddd = self.cleaned_data.get('ddd')

        if not ddd.isnumeric():
            raise ValidationError(_('DDD inválido'))

        return ddd

    def clean_number(self):

        number = self.cleaned_data.get('number')

        if not number.isnumeric():
            raise ValidationError(_('Número inválido'))

        return number

    class Meta:
        model = Phone
        fields = ['ddi', 'ddd', 'number', 'customer']


class CustomerEmailForm(forms.ModelForm):

    class Meta:
        model = CustomerEmail
        fields = ['customer', 'email']
