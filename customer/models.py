from django.core.validators import get_available_image_extensions, FileExtensionValidator
from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from commom.choices import DOC_TYPES_PJ, DOC_TYPES_PF
from commom.models import AbstractModel, AbstractSyncModel, AbstractDocument

from .choices import GENDER_CUSTOMER, SYSTEM_ORIGIN, CUSTOMER_ORIGIN

USER_MODEL = get_user_model()


class Customer(AbstractSyncModel):
    first_name = models.CharField(max_length=50, help_text=_('Primeiro nome'))
    last_name = models.CharField(max_length=50, help_text=_('Primeiro nome'))
    user = models.ForeignKey(USER_MODEL, null=True, help_text=_('Usuário'),
                             on_delete=models.PROTECT, blank=True)
    gender = models.CharField(choices=GENDER_CUSTOMER, max_length=6, help_text=_('Sexo do cliente'))
    origin = models.CharField(
        choices=CUSTOMER_ORIGIN, default=SYSTEM_ORIGIN, editable=False, max_length=6,
        help_text=_('Origem do registro, se é de sincronização ou do sistema')
    )

    @property
    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def is_company(self):
        if not hasattr(self, '_is_type'):
            company_doc = [f[0] for f in DOC_TYPES_PJ]
            for document in self.documents.all():
                # Como um PJ não pode salvar dados de PF e vice versa apenas o
                # primeiro registro já é o suficiente
                if document.doc_type in company_doc:
                    self._is_type = 'PJ'
                    break
        return getattr(self, '_is_type', '') == 'PJ'

    @property
    def is_person(self):
        if not hasattr(self, '_is_type'):
            person_doc = [f[0] for f in DOC_TYPES_PF]
            for document in self.documents.all():
                # Como um PJ não pode salvar dados de PF e vice versa apenas o
                # primeiro registro já é o suficiente
                if document.doc_type in person_doc:
                    self._is_type = 'PF'
                    break
        return getattr(self, '_is_type', '') == 'PF'

    def __str__(self):
        return self.get_full_name

    class Meta(AbstractModel.Meta):
        verbose_name = _('Cliente')


class CustomerDocument(AbstractSyncModel, AbstractDocument):
    customer = models.ForeignKey(Customer, help_text=_('Cliente'), on_delete=models.CASCADE,
                                 related_name='documents')
    file = models.FileField(help_text=_('Arquivo'), validators=[FileExtensionValidator(
        allowed_extensions=['pdf'] + get_available_image_extensions()
    )], upload_to='customer/documents', null=True, blank=True)

    class Meta(AbstractDocument.Meta):
        verbose_name = _('Documento do cliente')
        verbose_name_plural = _('Documentos do cliente')
        unique_together = ['customer', 'doc_type']


class Phone(AbstractModel):
    customer = models.ForeignKey(Customer, help_text=_('Cliente'), on_delete=models.CASCADE,
                                 related_name='phones')
    ddi = models.CharField(max_length=3, help_text=_('Código do país'))
    ddd = models.CharField(max_length=2, help_text=_('Código do estado'))
    number = models.CharField(max_length=9, help_text=_('Número do telefone'))

    class Meta(AbstractDocument.Meta):
        verbose_name = _('Telefone')
        unique_together = ['customer', 'ddi', 'ddd', 'number']


class CustomerEmail(AbstractModel):
    customer = models.ForeignKey(Customer, help_text=_('Cliente'), on_delete=models.CASCADE,
                                 related_name='emails')
    email = models.EmailField(help_text=_('Endereço de e-mail'))

    class Meta(AbstractDocument.Meta):
        verbose_name = _('E-mail')
        unique_together = ['customer', 'email']
