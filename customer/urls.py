from rest_framework_extensions.routers import ExtendedSimpleRouter

from .views import (
    CustomerViewSet, CustomerDocumentViewSet, PhoneViewSet, CustomerEmailViewSet
)

app_name = 'customer'

router = ExtendedSimpleRouter()
router_customer = router.register('customers', CustomerViewSet, 'customer')
router_customer.register('documents', CustomerDocumentViewSet, 'document',
                         parents_query_lookups=['customer'])
router_customer.register('phones', PhoneViewSet, 'customer-phone',
                         parents_query_lookups=['customer'])
router_customer.register('emails', CustomerEmailViewSet, 'email',
                         parents_query_lookups=['customer'])

urlpatterns = router.urls
