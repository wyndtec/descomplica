from django.utils.translation import gettext as _


MALE = 'male'
FEMALE = 'female'

GENDER_CUSTOMER = (
    (MALE, _('Homem')),
    (FEMALE, 'Mulher')
)

SYSTEM_ORIGIN = 'system'
SYNC_ORIGIN = 'sync'

CUSTOMER_ORIGIN = (
    (SYSTEM_ORIGIN, _('Sistema')),
    (SYNC_ORIGIN, _('Sincronização'))
)
