DOCKER_COMPOSE_FILE=conf/docker-compose.yml
DOCKERFILE=conf/Dockerfile

.PHONY: up
up:
	@echo "Initiliazing services"
	docker-compose -f $(DOCKER_COMPOSE_FILE) up -d --remove-orphans;
	@make services
	@echo "Await 2 seconds in initiliazing database"
	sleep 2
	@make setup_db


.PHONY: stop
stop:
	docker-compose -f $(DOCKER_COMPOSE_FILE) stop;


.PHONY: setup_db
setup_db:
	@make update_db
	./manage.py loaddata project/fixtures/000_site_dev
	./manage.py loaddata project/fixtures/001_admin
	@echo "\nRun: make admin-ui"
	@echo "Credentials:\n  - user: admin\n  - pass: 123"

.PHONY: update_db
update_db:
	./manage.py makemigrations
	./manage.py migrate

.PHONY: down
down:
	docker-compose -f $(DOCKER_COMPOSE_FILE) down --remove-orphans;


.PHONY: prune
prune:
	docker volume prune -f;


.PHONY: services
services:
	docker-compose -f $(DOCKER_COMPOSE_FILE) ps


.PHONY: logs
logs:
	docker-compose -f $(DOCKER_COMPOSE_FILE) logs -f


.PHONY: admin-ui
admin-ui:
	@echo "Opening Administration UI"
	x-www-browser http://localhost:8000/admin


.PHONY: build
build:
	docker build -f $(DOCKERFILE) -t descomplica-vendas .


.PHONY: clear-migrations
clear-migrations:
	find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "*/.env/*" -not -path "*/venv/*" -not -path "*/.venv/*" -delete
