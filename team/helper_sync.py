from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

from synchronizer.services.brokers import BrokersService

from core.util.string import clear_string
from enterprise.models import Seller

from team.models import TeamMember
from team.forms import TeamMemberForm, MemberDocumentForm

from commom.sync import HelperSync

USERNAME = settings.USERNAME_SANKHYA
PASSWORD = settings.PASSWORD_SANKHYA
USER_MODEL = get_user_model()


class TeamSync(HelperSync):

    def get_broker(cls, cpf_cnpj: str = None) -> dict:
        """
        Pega todas as informações da Sankhya referente aos corretores
        :param cpf_cnpj: cpf_cnpj a ser consultado
        """
        project = BrokersService()
        project.authenticate(username=USERNAME, password=PASSWORD)
        return project.fetch(cpf_cnpj)

    def convert_sync_to_model(cls, data: dict) -> dict:
        """ Converte os dados vindo do sincronizador para os campos do model """

        name_split = data['name'].split()
        first_name = name_split[0]
        last_name = ' '.join(name_split[1:])

        documents = []
        phones = []
        emails = []

        for document in data['documents']:
            documents.append({
                'doc_type': document['type'],
                'doc_number': document['number'],
            })

        for phone in data['phones']:
            p = clear_string(phone)
            if len(p) >= 11:
                phones.append({
                    'ddi': '55',
                    'ddd': p[:2],
                    'number': p[2:]
                })

        for email in data.get('emails'):
            if email:
                emails.append(email)

        return {
            'user': {
                'first_name': first_name,
                'last_name': last_name,
                'email': emails[0] if len(emails) > 0 else None
            },
            'external_id': data['id'],
            'documents': documents,
            'phones': phones,
            'emails': emails
        }

    def process_sync(self):
        """ Processa a sincronização """

        # Armazena todos os corretores cadastrados no sistema
        all_members = TeamMember.objects.filter(external_id__isnull=False).select_related('user')
        # Armazena todos os ids externos
        external_id_members = [r.external_id for r in all_members]

        # Armazena todos os dados vindo da Sankhya
        sync_all_data = self.get_broker()

        for sync_data in sync_all_data:
            data = self.convert_sync_to_model(sync_data)

            # O endereço de e-mail é obrigatório, senão tiver, não importa
            if not data['user']['email']:
                continue

            cpf_cnpj = [d for d in data['documents'] if d['doc_type'] in ['cpf', 'cnpj']][0]['doc_number']

            # Senão tiver sido encontrado o cpf_cnpj, ignora o registro
            if not cpf_cnpj:
                continue

            if data['external_id'] in external_id_members:
                member = [l for l in all_members if l.external_id == data['external_id']][0]

                # Atualiza os dados
                member.user.email = data['user']['email']
                member.user.username = member.user.email
                member.user.save()

                for document in data['documents']:
                    if document['doc_number'] and document['doc_type']:
                        member_doc = member.documents.filter(doc_type=document['doc_type'])
                        for doc in member_doc:
                            data = dict(document, **{'uuid': str(doc.uuid), 'member': str(doc.member.pk)})
                            form = MemberDocumentForm(data=data, instance=doc)
                            if form.is_valid():
                                document = form.save()
                            else:
                                raise ValidationError(form.errors)

            else:  # Não está cadastrado, crio o usuário e um registro de membro

                # TODO: alterar para criar um usuário comum e não um superusuario
                user = USER_MODEL.objects.create_superuser(
                    username=data['user']['email'],
                    email=data['user']['email'],
                    password=cpf_cnpj
                )

                form = TeamMemberForm(data={'user': user, 'active': True})
                if form.is_valid():
                    member = form.save()
                    member.external_id = data['external_id']
                else:
                    raise ValidationError(form.errors)

                # Criando os documentos do corretor
                for document in data['documents']:
                    if document['doc_number'] and document['doc_type']:
                        form = MemberDocumentForm(data=dict(document, **{'member': member}))
                        if form.is_valid():
                            document = form.save()
                        else:
                            raise ValidationError(form.errors)

                # TODO: Comentar quando tiver pronto a gestão de equipes
                seller = Seller()
                seller.member = member
                seller.save(ignore_validation=True)

            member.last_sync = timezone.now()
            member.validate()
            member.save()
