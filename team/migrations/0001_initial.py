# Generated by Django 3.0.8 on 2020-08-06 17:24

import core.models.mixins.deletable_mixin
import core.models.mixins.domain_ruler_mixin
import core.models.mixins.entity_mixin
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('name', models.CharField(help_text='Nome da equipe', max_length=50)),
                ('avatar', models.ImageField(help_text='Avatar da equipe', upload_to='team/avatars')),
                ('supervisor', models.OneToOneField(blank=True, help_text='Supervisor da equipe', null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Equipe de venda',
                'verbose_name_plural': 'Equipes de vendas',
                'ordering': ['-created_at'],
                'abstract': False,
            },
            bases=(core.models.mixins.entity_mixin.EntityMixin, core.models.mixins.deletable_mixin.DeletableModelMixin, core.models.mixins.domain_ruler_mixin.DomainRuleMixin, models.Model),
        ),
        migrations.CreateModel(
            name='TeamMember',
            fields=[
                ('active', models.BooleanField(default=True, verbose_name='active')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('external_id', models.IntegerField(blank=True, editable=False, null=True, unique=True, verbose_name='ID que identifica o registro na Sankhya')),
                ('last_sync', models.DateTimeField(editable=False, null=True, verbose_name='última sincronização')),
                ('team', models.ForeignKey(blank=True, help_text='Nome da equipe', null=True, on_delete=django.db.models.deletion.PROTECT, to='team.Team')),
                ('user', models.OneToOneField(help_text='Usuário do membro', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Membro de equipe',
                'verbose_name_plural': 'Membros de equipes',
                'ordering': ['-created_at'],
                'abstract': False,
            },
            bases=(core.models.mixins.entity_mixin.EntityMixin, core.models.mixins.deletable_mixin.DeletableModelMixin, core.models.mixins.domain_ruler_mixin.DomainRuleMixin, models.Model),
        ),
        migrations.CreateModel(
            name='MemberDocument',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('doc_type', models.CharField(choices=[('cpf', 'Número do CPF'), ('rg', 'RG'), ('cnpj', 'CNPJ'), ('ie', 'Inscrição Estadual'), ('im', 'Inscrição Municipal'), ('address', 'Comprovante de endereço'), ('creci', 'Conselho Regional de Corretores de Imóveis')], help_text='Tipo de documento', max_length=7)),
                ('doc_number', models.CharField(max_length=14)),
                ('member', models.ForeignKey(help_text='Membro', on_delete=django.db.models.deletion.CASCADE, related_name='documents', to='team.TeamMember')),
            ],
            options={
                'verbose_name': 'Documento do corretor',
                'verbose_name_plural': 'Documentos dos corretores',
                'ordering': ['-created_at'],
                'abstract': False,
                'unique_together': {('member', 'doc_type')},
            },
            bases=(core.models.mixins.entity_mixin.EntityMixin, core.models.mixins.deletable_mixin.DeletableModelMixin, core.models.mixins.domain_ruler_mixin.DomainRuleMixin, models.Model),
        ),
    ]
