from rest_framework_extensions.routers import ExtendedSimpleRouter, ExtendedDefaultRouter

from .views import TeamViewSet, TeamMemberViewSet, MemberDocumentViewSet

app_name = 'team'

router = ExtendedDefaultRouter()
router_team = router.register('teams', TeamViewSet, 'team')
router_member = router.register('members', TeamMemberViewSet, 'member')
router_member.register('documents', MemberDocumentViewSet, 'member-documents',
                       parents_query_lookups=['member'])

urlpatterns = router.urls
