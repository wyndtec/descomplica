from commom.viewset import PermissionModelViewSet

from .models import Team, TeamMember, MemberDocument
from .serializers import TeamSerializer, TeamMemberSerializer, MemberDocumentSerializer

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.filters import SearchFilter

from rest_framework_extensions.mixins import NestedViewSetMixin

from djangorestframework_camel_case.parser import CamelCaseMultiPartParser, CamelCaseJSONParser


class TeamViewSet(PermissionModelViewSet):
    queryset = Team.objects.filter()
    serializer_class = TeamSerializer
    filter_backends = [SearchFilter]
    search_fields = ['name']
    parser_classes = [CamelCaseMultiPartParser, CamelCaseJSONParser]


class TeamMemberViewSet(PermissionModelViewSet):
    queryset = TeamMember.objects.filter()
    serializer_class = TeamMemberSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user', 'active', 'team']


class MemberDocumentViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = MemberDocument.objects.filter()
    serializer_class = MemberDocumentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['member']
