from rest_framework.serializers import ModelSerializer

from .models import Team, TeamMember, MemberDocument
from .forms import TeamForm, TeamMemberForm, MemberDocumentForm

from core.serializers.form_serializer_mixin import FormSerializerMixin


class TeamSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = TeamForm
        model = Team
        fields = ['uuid'] + TeamForm.Meta.fields


class TeamMemberSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = TeamMemberForm
        model = TeamMember
        fields = ['uuid'] + TeamMemberForm.Meta.fields


class MemberDocumentSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = MemberDocumentForm
        model = MemberDocument
        fields = ['uuid'] + MemberDocumentForm.Meta.fields
