from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from commom.forms import DocumentForm
from .models import Team, TeamMember, MemberDocument


class TeamForm(forms.ModelForm):

    class Meta:
        model = Team
        fields = ['name', 'avatar', 'supervisor']


class TeamMemberForm(forms.ModelForm):

    class Meta:
        model = TeamMember
        fields = ['team', 'user', 'active']


class MemberDocumentForm(DocumentForm):

    def get_all_documents(self):
        member = self.cleaned_data.get('member')
        return member.documents.all()

    def get_owner_documents(self):
        return self.cleaned_data.get('customer')

    def clean_member(self):
        member = self.cleaned_data.get('member')
        if 'member' in self.changed_data and not self.instance.is_new:
            if self.instance.member != member:
                raise ValidationError(
                    _('Não é possível alterar o member dono do documento')
                )

        return member

    class Meta:
        model = MemberDocument
        fields = ['member', 'doc_type', 'doc_number']
