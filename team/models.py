from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from commom.models import AbstractModel, AbstractSyncModel, AbstractDocument
from commom.choices import BROKERS_DOC, DOC_TYPES
from core.models.mixins import ActivableMixin

USER_MODEL = get_user_model()


class Team(AbstractModel):
    name = models.CharField(max_length=50, help_text=_('Nome da equipe'))
    avatar = models.ImageField(help_text=_('Avatar da equipe'), upload_to='team/avatars')
    # TODO: alterar assim que existir a figura do supervisor no sistema
    supervisor = models.OneToOneField(USER_MODEL, help_text=_('Supervisor da equipe'),
                                      on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta(AbstractModel.Meta):
        verbose_name = _('Equipe de venda')
        verbose_name_plural = _('Equipes de vendas')


class TeamMember(AbstractSyncModel, ActivableMixin):
    # TODO: temporariamente, não obrigatório
    team = models.ForeignKey(Team, help_text=_('Nome da equipe'),
                             on_delete=models.PROTECT, null=True, blank=True)
    user = models.OneToOneField(USER_MODEL, help_text=_('Usuário do membro'),
                                on_delete=models.PROTECT, related_name='members')

    def __str__(self):
        return f'{self.user} | {self.team}'

    class Meta(AbstractModel.Meta):
        verbose_name = _('Membro de equipe')
        verbose_name_plural = _('Membros de equipes')


class MemberDocument(AbstractModel, AbstractDocument):
    member = models.ForeignKey(TeamMember, help_text=_('Membro'), on_delete=models.CASCADE,
                               related_name='documents')
    doc_type = models.CharField(
        choices=DOC_TYPES + BROKERS_DOC, max_length=7, help_text=_('Tipo de documento')
    )
    doc_number = models.CharField(max_length=14)

    class Meta(AbstractDocument.Meta):
        verbose_name = _('Documento do corretor')
        verbose_name_plural = _('Documentos dos corretores')
        unique_together = ['member', 'doc_type']
