from django.contrib import admin

from .forms import TeamForm, TeamMemberForm, MemberDocumentForm
from .models import Team, TeamMember, MemberDocument


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = (
        'uuid',
        'created_at',
        'updated_at',
        'name',
        'avatar',
        'supervisor',
    )
    list_filter = ('created_at', 'updated_at', 'supervisor')
    search_fields = ('name',)
    date_hierarchy = 'created_at'
    form = TeamForm


@admin.register(TeamMember)
class TeamMemberAdmin(admin.ModelAdmin):
    list_display = (
        'active',
        'uuid',
        'created_at',
        'updated_at',
        'team',
        'user',
    )
    list_filter = ('active', 'created_at', 'updated_at', 'team', 'user')
    date_hierarchy = 'created_at'
    form = TeamMemberForm


@admin.register(MemberDocument)
class MemberDocumentAdmin(admin.ModelAdmin):
    list_display = (
        'uuid',
        'created_at',
        'updated_at',
        'member',
        'doc_type',
        'doc_number',
    )
    list_filter = ('created_at', 'updated_at', 'member')
    date_hierarchy = 'created_at'
    form = MemberDocumentForm
