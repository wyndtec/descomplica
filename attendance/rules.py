from django.utils.translation import gettext as _

from core.models.mixins import IntegrityRuleChecker, RuleIntegrityError


class AttendanceResponsibleValidated(IntegrityRuleChecker):
    """ Valida se está correto o preenchimento do responsável pelo atendimento """

    def check(self, instance) -> None:
        if instance.manager and instance.seller:
            raise RuleIntegrityError(
                message=_('Um atendimento não pode ter um gerente e um vendedor vinculado ao mesmo'),
                field_name='manager'
            )

        if not instance.manager and not instance.seller:
            raise RuleIntegrityError(
                message=_('Não foi informado o responsável pelo atendimento!')
            )
