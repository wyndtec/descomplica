from commom.viewset import PermissionModelViewSet

from rest_framework_extensions.mixins import NestedViewSetMixin

from .models import Attendance, CommercialNote
from .serializers import AttendanceSerializer, CommercialNoteSerializer

from rest_framework.filters import SearchFilter

from django_filters.rest_framework import DjangoFilterBackend
from django_filters import FilterSet, UUIDFilter

from django.utils.translation import gettext as _


class AttendanceFilter(FilterSet):
    project = UUIDFilter(field_name='lot__project', label=_('Empreendimento'))

    class Meta:
        model = Attendance
        fields = ['manager', 'customer', 'seller', 'project']


class CommercialNoteFilter(FilterSet):
    project = UUIDFilter(field_name='attendance__lot__project', label=_('Empreendimento'))

    class Meta:
        model = CommercialNote
        fields = ['attendance', 'project']


class AttendanceViewSet(PermissionModelViewSet):
    queryset = Attendance.objects.filter()
    serializer_class = AttendanceSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = AttendanceFilter


class CommercialNoteViewSet(NestedViewSetMixin, PermissionModelViewSet):
    queryset = CommercialNote.objects.filter()
    filter_backends = [SearchFilter, DjangoFilterBackend]
    search_fields = ['text']
    serializer_class = CommercialNoteSerializer
    filterset_class = CommercialNoteFilter
