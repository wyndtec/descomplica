from django.utils.translation import gettext as _

ATTENDANCE_COURSE = 'course'
ATTENDANCE_FINISHED = 'finished'
ATTENDANCE_QUEUE = 'queue'
ATTENDANCE_RESERVED = 'reserved'
ATTENDANCE_PROPOSAL = 'proposal'
ATTENDANCE_CLOSED = 'closed'

ATTENDANCE_STATUS = (
    (ATTENDANCE_COURSE, _('Em andamento')),
    (ATTENDANCE_FINISHED, _('Encerrado')),
    (ATTENDANCE_QUEUE, _('Em fila')),
    (ATTENDANCE_RESERVED, _('Em reserva')),
    (ATTENDANCE_PROPOSAL, _('Em proposta')),
    (ATTENDANCE_CLOSED, _('Finalizado')),
)
