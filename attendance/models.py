from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from attendance.rules import AttendanceResponsibleValidated
from attendance.choices import ATTENDANCE_STATUS, ATTENDANCE_COURSE

from commom.models import AbstractModel

from enterprise.models import Lot, Seller
from customer.models import Customer

USER_MODEL = get_user_model()


class Attendance(AbstractModel):
    """"
     - Um atendimento não pode estar vinculado a um vendedor e a um gerente.

     - Caso não tenha um vendedor, o atendimento é redirecionado para o gerente
     do vendedor anterior.

     - expired_at tem o seu valor alterado de acordo com o status:
        * Caso o status é em reserva:
            + O seu valor será a data de expiração da reserva
        * Caso o status é em fila:
            + O seu valor será a data de expiração da confirmação da reserva,
            caso ele esteja na posição 1
        * Caso o status seja em andamento:
            + Será a data de expiração do atendimento
        * Caso o status seja em proposta:
            + Terá o seu valor vazio
     """
    lot = models.ForeignKey(Lot, help_text=_('Lote'), on_delete=models.PROTECT, related_name='attendances')
    customer = models.ForeignKey(Customer, help_text=_('Cliente'), on_delete=models.PROTECT, related_name='attendances')
    status = models.CharField(max_length=8, choices=ATTENDANCE_STATUS, default=ATTENDANCE_COURSE,
                              help_text=_('Estado atual do atendimento'))
    expired_at = models.DateTimeField(null=True, blank=True, editable=False,
                                      help_text=_('Data/hora de espiração'))
    queue_position = models.IntegerField(default=0, help_text=_('Posição da fila'),
                                         editable=False)
    # TODO: alterar assim que existir a figura de manager no sistema
    manager = models.ForeignKey(USER_MODEL, help_text=_('Gerente responsável pelo atendimento'),
                                on_delete=models.PROTECT, null=True, blank=True)
    seller = models.ForeignKey(Seller, help_text=_('Vendedor responsável'), on_delete=models.PROTECT, null=True, blank=True)

    integrity_rules = [
        AttendanceResponsibleValidated
    ]

    @property
    def responsible(self):
        """ Retorna quem é o responsável pelo atendimento """
        if self.manager is None:
            return self.seller
        else:
            return self.manager

    def __str__(self):
        return f'{self.customer}|{self.lot}|{self.responsible}'

    class Meta(AbstractModel.Meta):
        verbose_name = _('Atendimento')


class CommercialNote(AbstractModel):
    attendance = models.ForeignKey(Attendance, help_text=_('Atendimento'), on_delete=models.PROTECT,
                                   related_name='comercial_notes')
    text = models.TextField(help_text=_('Descrição do atendimento'))

    class Meta(AbstractModel.Meta):
        verbose_name = _('Notas comerciais')
