from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from enterprise.choices import LOT_RESERVED, LOT_AVAILABLE
from enterprise.helper_sync import ReservedSync
from .choices import ATTENDANCE_CLOSED, ATTENDANCE_FINISHED, ATTENDANCE_RESERVED
from .models import Attendance, CommercialNote


class AttendanceForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AttendanceForm, self).__init__(*args, **kwargs)

        if self.instance.is_new:
            self.fields['lot'].queryset = self.fields['lot'].queryset.filter(status=LOT_AVAILABLE)
        else:
            self.fields['lot'].queryset = self.fields['lot'].queryset.filter(pk=self.instance.lot_id)

    def clean_lot(self):
        lot = self.cleaned_data.get('lot')

        if not lot.project.active:
            raise ValidationError(_('Não é possível criar um atendimento para um empreendimento inativo'))

        if not self.instance.is_new and 'lot' in self.changed_data:
            if self.instance.lot.project != lot.project:
                raise ValidationError(
                    _('Não é permitido mudar o atendimento de empreendimento')
                )

        return lot

    def clean_customer(self):
        customer = self.cleaned_data.get('customer')
        status = self.cleaned_data.get('status')

        if status == ATTENDANCE_RESERVED:
            if not customer.documents.filter(doc_type__in=['cpf', 'cnpj']).exists():
                raise ValidationError(
                    _('O cadastro do cliente está incompleto! Informar o cpf/cnpj do mesmo')
                )

        return customer

    def clean(self):
        if not self.instance.is_new and not self.errors:
            if self.instance.status in (ATTENDANCE_CLOSED, ATTENDANCE_FINISHED):
                raise ValidationError(
                    _('Um atendimento finalizado não pode ser alterado!')
                )

            # Está alterando o status do attendimento para em reserva
            if self.instance.status != ATTENDANCE_RESERVED and \
                    self.cleaned_data.get('status') == ATTENDANCE_RESERVED:
                self.instance.lot.status = LOT_RESERVED
                self.instance.lot.save(ignore_validation=True)

                sync = ReservedSync()
                try:
                    sync.process_sync(self.instance)
                except Exception as e:
                    raise ValidationError(_(f'{e}'))

        return super(AttendanceForm, self).clean()

    class Meta:
        model = Attendance
        fields = ['lot', 'manager', 'seller', 'status', 'customer']


class CommercialNoteForm(forms.ModelForm):

    class Meta:
        model = CommercialNote
        fields = ['text', 'attendance']
