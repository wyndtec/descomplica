from rest_framework_extensions.routers import ExtendedSimpleRouter

from .views import AttendanceViewSet, CommercialNoteViewSet

app_name = 'attendance'

router = ExtendedSimpleRouter()
router_attendance = router.register('attendances', AttendanceViewSet, 'attendance')
router.register('commercialnotes', CommercialNoteViewSet, 'commercialnote')

urlpatterns = router.urls
