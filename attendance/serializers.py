from rest_framework.serializers import ModelSerializer

from .models import Attendance, CommercialNote
from .forms import AttendanceForm, CommercialNoteForm

from core.serializers.form_serializer_mixin import FormSerializerMixin


class AttendanceSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = AttendanceForm
        model = Attendance
        fields = [
            'uuid', 'status', 'expired_at', 'queue_position', 'created_at', 'updated_at'
        ] + AttendanceForm.Meta.fields


class CommercialNoteSerializer(FormSerializerMixin, ModelSerializer):

    class Meta:
        form = CommercialNoteForm
        model = CommercialNote
        fields = [
            'uuid', 'created_at', 'updated_at'
        ] + CommercialNoteForm.Meta.fields
