from django.contrib import admin

from .models import Attendance, CommercialNote

from .forms import AttendanceForm, CommercialNoteForm


@admin.register(Attendance)
class AttendanceAdmin(admin.ModelAdmin):
    list_display = (
        'uuid',
        'created_at',
        'updated_at',
        'lot',
        'customer',
        'status',
        'expired_at',
        'queue_position',
        'manager',
        'responsible'
    )
    list_filter = (
        'created_at',
        'updated_at',
        'expired_at',
    )
    date_hierarchy = 'created_at'
    form = AttendanceForm

    def get_field_queryset(self, db, db_field, request):
        if db_field.name == 'lot':
            qs = super(AttendanceAdmin, self).get_field_queryset(db, db_field, request)
            if qs is None:
                qs = db_field.remote_field.model._default_manager.using(db)
            return qs.select_related('project')
        else:
            return super(AttendanceAdmin, self).get_field_queryset(db, db_field, request)


@admin.register(CommercialNote)
class CommercialNoteAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'created_at', 'updated_at', 'attendance', 'text')
    list_filter = ('created_at', 'updated_at', 'attendance')
    date_hierarchy = 'created_at'
    form = CommercialNoteForm
